<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S05: Client-Server Communication (Basic To-Do)</title>
</head>
<body>
	<?php session_start() ?>

	
	<?php if (isset($_SESSION['tasks']) === false): ?>
		<h3>Login</h3>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="add" />
			Email: <input type="text" name="email" required/>
			Password: <input type="text" name="password" required/>
			<button type="submit">Login</button>
		</form>
	<?php endif; ?>
	

	<?php if (isset($_SESSION['tasks'])): ?>
		<?php foreach($_SESSION['tasks'] as $index => $task): ?>
			<?php if ($task->email === "johnsmith@gmail.com" && $task->password === "1234"): ?>
				<h3>Hello <?php echo $task->email ?></h3>
				<br><br>
				<form method="POST" action="./server.php">
					<input type="hidden" name="action" value="clear">
					<button type="submit">Logout</button>
				</form>
			<?php endif; ?>	
			<?php if ($task->email !== "johnsmith@gmail.com" && $task->password !== "1234"): ?>
				<h3>Login ERROR!</h3>
				<br>
				<form method="POST" action="./server.php">
					<input type="hidden" name="action" value="clear">
					<button type="submit">Go back</button>
				</form>
			<?php endif; ?>	
		<?php endforeach; ?>
	<?php endif; ?>
</body>
</html>

<!-- 	<form method="POST" action="./server.php">
		Email: <input type="email" name="email" required/>
		Password: <input type="password" name="password" required/>
		<button type="submit" name="login" value="login">Login</button>
	</form> -->