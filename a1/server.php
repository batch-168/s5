<?php

session_start();

class TaskList {
	public function add($email, $password){
		$newTask = (object)[
			'email' => $email,
			'password' => $password,
			'isFinished' => false
		];

		if($_SESSION['tasks'] === null){
			$_SESSION['tasks']= array();
		}

		array_push($_SESSION['tasks'], $newTask);
	}

	public function clear(){
		session_destroy();
	}	
}

$taskList = new TaskList();

if($_POST['action'] === 'add'){
	// add function
	$taskList->add($_POST['email'], $_POST['password']);
}
else if($_POST['action'] === 'clear'){
	$taskList->clear();
}

header('Location: ./index.php');
