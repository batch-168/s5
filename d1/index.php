<?php
	$tasks = ['Get git', 'bake html', 'Eat css', 'Learn php'];
	if (isset($_GET['index'])) {
		$indexGet = $_GET['index'];
		echo "The retrieved task from GET is $tasks[$indexGet] ";
	}
	if (isset($_POST['index'])) {
		$indexPost = $_POST['index'];
		echo "The retrieved task from POST is $tasks[$indexPost]";
	}
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S05: Client-Server Communication (GET and POST)</title>
</head>
<body>
	<h1>Task index from Get</h1>

	<form method="GET">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
	<button type="submit">GET</button>
	</form>

	<h1>Task index from Post</h1>

	<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			
		</select>
	<button type="submit">POST</button>
	</form>

	<p><?php $condominium->setName('Enzo Tower') ?></p>

</body>
</html>